import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';
import { HttpClientModule } from '@angular/common/http';
import { IonicStorageModule } from '@ionic/storage';

import { AddSightPage } from '../pages/add-sight/add-sight';
import { AddBirdPage } from '../pages/add-bird/add-bird';
import { LoginPage } from '../pages/login/login';
import { MenuPage } from '../pages/menu/menu';
import { HomePage } from '../pages/home/home';
import { DetailPage } from '../pages/detail/detail';

import { LoginPageModule } from '../pages/login/login.module';
import { HomePageModule } from '../pages/home/home.module';
import { MenuPageModule } from '../pages/menu/menu.module';
import { DetailPageModule } from '../pages/detail/detail.module';
import { AddSightPageModule } from '../pages/add-sight/add-sight.module';
import { AddBirdPageModule } from '../pages/add-bird/add-bird.module';

import { Geolocation } from '@ionic-native/geolocation';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { Provider } from '../providers/provider/provider';

@NgModule({
  declarations: [
    MyApp
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    HttpClientModule,
    LoginPageModule,
    HomePageModule,
    MenuPageModule,
    DetailPageModule,
    AddSightPageModule,
    AddBirdPageModule,
    IonicStorageModule.forRoot()
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    LoginPage,
    AddSightPage,
    DetailPage,
    AddBirdPage,
    MenuPage,
    HomePage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    Geolocation,
    Provider,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
