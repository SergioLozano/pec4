import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { Storage } from '@ionic/storage';

/*
  Generated class for the Provider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class Provider {


  constructor(public http: HttpClient, private storage: Storage) {
    console.log('Hello Provider Provider');
  }

  getBirdList(user){
      return this.http.get('http://dev.contanimacion.com/birds/public/getBirds/' + user);
  }

  getBirdDetail(birdId){
      return this.http.get('http://dev.contanimacion.com/birds/public/getBirdDetails/' + birdId);
  }

  getCredentials(postData){
      return this.http.post('http://dev.contanimacion.com/birds/public/login/', postData);
  }

  postBird(bird){
      return this.http.post('http://dev.contanimacion.com/birds/public/addBird/', bird);
  }

  postSight(sight){
      return this.http.post('http://dev.contanimacion.com/birds/public/addSighting/', sight);
  }




}
