import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { Provider } from '../../providers/provider/provider';
import { Storage } from '@ionic/storage';

@IonicPage()
@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  birds

  constructor(public navCtrl: NavController, public provider:Provider, private storage: Storage) {}

  ionViewDidLoad(){
      this.storage.get('id').then((val) => {
          if(val == null){
              this.navCtrl.setRoot('LoginPage');
          } else{
              console.log(val)
              this.provider.getBirdList(val).subscribe(
                  (data) => {this.birds = data;},
                  (error) => {console.log(error);}
              )
          }
      })

  }

  goToDetail(birdId){
      this.navCtrl.push('DetailPage',{
         birdId: birdId
      });
  }



}
