import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Provider } from '../../providers/provider/provider';



@IonicPage()
@Component({
  selector: 'page-detail',
  templateUrl: 'detail.html',
})
export class DetailPage {
  bird;
  birdId;

  constructor(public navCtrl: NavController, public navParams: NavParams,  public provider:Provider) {
      this.birdId = navParams.get('birdId');
  }

  ionViewDidLoad() {
      this.provider.getBirdDetail(this.birdId).subscribe(
          (data) => {
              this.bird = data;
          },
          (error) => {console.log(error);}
      )
  }

  addBird(){
      this.navCtrl.push('AddSightPage',{
         birdId: this.birdId
      });
  }

}
