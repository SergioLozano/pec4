import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AddSightPage } from './add-sight';

@NgModule({
  declarations: [
    AddSightPage,
  ],
  imports: [
    IonicPageModule.forChild(AddSightPage),
  ],
})
export class AddSightPageModule {}
