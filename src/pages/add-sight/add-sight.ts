import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Geolocation } from '@ionic-native/geolocation';
import { Provider } from '../../providers/provider/provider';

/**
 * Generated class for the AddSightPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-add-sight',
  templateUrl: 'add-sight.html',
})
export class AddSightPage {

  birdId;
  disableBtn: boolean = true;
  place: string;
  lat: number;
  long: number;


  constructor(public navCtrl: NavController, public navParams: NavParams, public provider:Provider, private geolocation: Geolocation) {
      this.birdId = navParams.get('birdId');
  }

  ionViewDidLoad() {
      console.log(this.birdId);
      this.geolocation.getCurrentPosition().then((resp) => {
        console.log("lat " + resp.coords.latitude);
        console.log("long " + resp.coords.longitude);
        this.lat = resp.coords.latitude;
        this.long = resp.coords.longitude;
        this.disableBtn = false;
    }).catch((error) => {
        console.log('Error getting location', error);
    });

  }

  addSight(){

      let postData = new FormData();
      postData.append('idAve' , this.birdId);
      postData.append('place' , this.place);
      postData.append('long' , this.long);
      postData.append('lat' , this.lat);


      this.provider.postSight(postData)
      .subscribe(response => {
          console.log(response.status);
          this.navCtrl.setRoot('MenuPage');
        },
        err => {
        console.log('Error: ' + err.message);
        });

        console.log(this.place);
  }

}
