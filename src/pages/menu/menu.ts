import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Storage } from '@ionic/storage';
/**
 * Generated class for the MenuPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-menu',
  templateUrl: 'menu.html',
})
export class MenuPage {

  constructor(public navCtrl: NavController, public navParams: NavParams, private storage: Storage) {
  }

  ionViewDidLoad() {
      this.storage.get('id').then((val) => {
          if(val == null){
              this.navCtrl.setRoot('LoginPage');
          }
      });
  }

  birdList(){
      this.navCtrl.push('HomePage');
  }

  addBird(){
      this.navCtrl.push('AddBirdPage');
  }

  logout(){
      this.storage.remove('id').then((val) => {
          if(val == null){
              this.navCtrl.setRoot('LoginPage');
          }
      });
  }

  info(){
      console.log("No action");
  }

}
