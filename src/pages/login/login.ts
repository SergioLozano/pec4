import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { User } from "../../models/user";
import { Provider } from '../../providers/provider/provider';
import { Storage } from '@ionic/storage';


@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {

  user = {} as User;

  constructor(
      public navCtrl: NavController,
      public navParams: NavParams,
      public provider:Provider,
      private storage: Storage) {
  }

  ionViewDidLoad() {
      this.storage.get('id').then((val) => {
          if(val != null){
              this.navCtrl.setRoot('MenuPage');
          }
      });
  }

  login(user: User){
      let postData = new FormData();
      postData.append('user' , user.email);
      postData.append('password' , user.password);

      this.provider.getCredentials(postData)
      .subscribe(response => {
        const store = this.storage.set('id', response.id);
        if(store){
            this.navCtrl.setRoot('MenuPage');
        }
        },
        err => {
        console.log('Error: ' + err.message);
        });

    }


}
