import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Bird } from "../../models/bird";
import { Provider } from '../../providers/provider/provider';
import { Geolocation } from '@ionic-native/geolocation';

/**
 * Generated class for the AddBirdPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-add-bird',
  templateUrl: 'add-bird.html',
})
export class AddBirdPage {

  bird = {} as Bird;
  sight: boolean;
  disableBtn: boolean = true;
  lat: number;
  long: number;

  constructor(public navCtrl: NavController, public navParams: NavParams, public provider:Provider, private geolocation: Geolocation) {
  }

  ionViewDidLoad() {
      this.geolocation.getCurrentPosition().then((resp) => {
        console.log("lat " + resp.coords.latitude);
        console.log("long " + resp.coords.longitude);
        this.lat = resp.coords.latitude;
        this.long = resp.coords.longitude;
    }).catch((error) => {
        console.log('Error getting location', error);
    });
  }

  updateSighted() {
    this.sight = !this.sight;
  }

  addBird(bird: Bird){
      let postData = new FormData();
      postData.append('idUser' , bird.idUser);
      postData.append('bird_name' , bird.bird_name);
      postData.append('bird_description' , bird.bird_description);

      if(this.sight && this.lat != null && this.long != null){
          postData.append('place' , bird.place);
          postData.append('lat' , this.lat);
          postData.append('long' , this.long);
      }


      this.provider.postBird(postData)
      .subscribe(response => {
          console.log(response.status);
          this.navCtrl.setRoot('MenuPage');
        },
        err => {
        console.log('Error: ' + err.message);
        });
  }

}
