export interface Bird {
    idUser: string;
    bird_name: string;
    bird_description: string;
    place: string;
    long: number;
    lat: number;
}
